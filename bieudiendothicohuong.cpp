#include <bits/stdc++.h>
using namespace std;
main(){
    int t;
    cin>>t;
    while(t--){
        int v,e;
        cin>>v>>e;
        vector<int> List[v+1];
        for(int i=0;i<e;i++){
            int a,b;
            cin>>a>>b;
            List[a].push_back(b);
        }
        for(int i=1;i<=v;i++)
            sort(List[i].begin(),List[i].end());
        for(int i=1;i<=v;i++){
            cout<<i<<':';
            for(int j=0;j<List[i].size();j++)
                cout<<' '<<List[i][j];
            cout<<endl;    
        }    
    }
    
}
