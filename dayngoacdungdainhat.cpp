#include <bits/stdc++.h>
using namespace std;
int tinh(string s){
    int n=s.length(),kq=0,i;
    stack<int> st;
    st.push(-1);
    for(i=0;i<n;i++){
        if(s[i]=='(') st.push(i);
        else{
            st.pop();
            if(!st.empty())
                kq= max(kq,i-st.top());
            else st.push(i);    
        }
    }
    return kq;
}
main(){
    int t;
    string s;
    cin>>t;
    while(t--){
        cin>>s;
        cout<<tinh(s)<<endl;
    }
}
